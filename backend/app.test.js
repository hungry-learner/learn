const request = require("supertest");

var app = require("./app");

it("should return Hello Test", function(done) {
    request(app)
        .get("/api/test")
        .expect({ title: "Express" })
        .end(done);
});
